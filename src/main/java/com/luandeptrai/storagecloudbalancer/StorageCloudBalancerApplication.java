package com.luandeptrai.storagecloudbalancer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class StorageCloudBalancerApplication {

    public static void main(String[] args) {
        SpringApplication.run(StorageCloudBalancerApplication.class, args);
    }

}
